# RimWorld Save Manager #

This is done for educational purposes, so don't expect it to be all so great (or good, for that matter).
But, it does do the basic skill/trait editing, and hopefully without (m)any issues.
It supports modded traits as well, and you can place it in your root game folder, or copy the Mods folder to the program's location.

#### Disclaimer: Use at your own risk, I am not responsible for your loss of progress. ####

### ChangeLog ###
#### v0.54 ####
* Added extra information to backstories (Trait bonuses and disabled skills)
#### v0.53 ####
* Fixed a bug in loading modded traits (Duplicates)
* Added default load/save path to platforms other than Windows
#### v0.52 ####
* Fixed another bug in backstory collection (Please post logs)
* Added the ability to modify biological age
#### v0.51 ####
* Fixed bug in backstory collection
#### v0.5 ####
* Added backstory editing (experimental)
#### v0.42 ####
* Fixed culture dependence in type conversion
#### v0.41 ####
* Fixed some loading bugs
* Added the ability to remove injuries and infections
#### v0.4 ####
* Updated to support Alpha 14
#### v0.3 ####
* Added initial support for injuries and infections
* Updated to support Alpha 13
#### v0.2 ####
* Added support for traits
#### v0.1 ####
* Basic editing of skills and passions